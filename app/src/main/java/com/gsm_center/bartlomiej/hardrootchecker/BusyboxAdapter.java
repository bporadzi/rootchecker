package com.gsm_center.bartlomiej.hardrootchecker;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gsm_center.bartlomiej.R;

import java.util.List;

/*
	Created by Bartek on 23.03.2018.
*/

public class BusyboxAdapter extends RecyclerView.Adapter <BusyboxAdapter.BusyboxViewHolder>
{

	private List <BusyboxInfo> busyboxInfo;

	BusyboxAdapter(List <BusyboxInfo> busyboxInfo)
	{
		this.busyboxInfo = busyboxInfo;
	}

	@Override
	public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
	{
		super.onAttachedToRecyclerView(recyclerView);
	}

	@NonNull
	@Override
	public BusyboxAdapter.BusyboxViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
			int viewType)
	{
		View v = LayoutInflater.from(parent.getContext()).inflate(R.
				layout.busybox_card, parent, false);
		v.setOnClickListener(cardClickListener);
		return new BusyboxAdapter.BusyboxViewHolder(v);
	}

	@Override
	public void onBindViewHolder(@NonNull BusyboxAdapter.BusyboxViewHolder holder, int position)
	{
		holder.cardTitle.setText(busyboxInfo.get(position).title);
		String message = busyboxInfo.get(position).message;

		if (message != null)
		{
			holder.cardMessage.setText(message);
			return;
		}

		StringBuilder messageBuilder = new StringBuilder();

		if (busyboxInfo.get(position).messages != null)
		{
			for (String mes : busyboxInfo.get(position).messages)
			{
				messageBuilder.append(mes);
			}
		}


		holder.cardMessage.setText(messageBuilder);

	}

	@Override
	public int getItemCount()
	{
		return busyboxInfo.size();
	}

	private View.OnClickListener cardClickListener = v -> {
		//			View message = v.findViewById(R.id.full_article);
		//
		//			message.setVisibility(message.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
		//
		//			TransitionManager.beginDelayedTransition(recyclerView);
		//			notifyDataSetChanged();
	};

	public static class BusyboxViewHolder extends RecyclerView.ViewHolder
	{

		CardView cv;
		TextView cardTitle;
		TextView cardMessage;

		BusyboxViewHolder(View itemView)
		{
			super(itemView);
			cv = itemView.findViewById(R.id.busybox_card);
			cardTitle = itemView.findViewById(R.id.title);
			cardMessage = itemView.findViewById(R.id.message);
		}

	}
}
