package com.gsm_center.bartlomiej.hardrootchecker;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.gsm_center.bartlomiej.R;

public class MainActivity extends AppCompatActivity
{

	private ViewPager viewPager;

	private RootFragment rootFragment;
	private BusyBoxFragment busyboxFragment;
	private Fragment moreFragment;

	private int lastFragmentIndex = 0;

	private BottomNavigationView navigation;

	private RootInfo rootInfo;

	public static boolean rootNotifyReceived = false;
	public static boolean busyboxNotifyReceived = false;

	private static final Object locker = new Object();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		viewPager = findViewById(R.id.pager);

		viewPager.addOnPageChangeListener(pageChangeListener);

		ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

		rootFragment = new RootFragment();
		busyboxFragment = new BusyBoxFragment();
		moreFragment = new InfoFragment();

		viewPager.setAdapter(viewPagerAdapter);

		viewPager.setOffscreenPageLimit(2);

		navigation = findViewById(R.id.navigation);
		navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
	}

	private void checkRoot()
	{
		rootInfo = new RootInfo(this.getApplicationContext());
		try
		{
			Data.canExecuteRootCommands = rootInfo.canExecuteSuCommand();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			getCheckingThread().start();
		}
	}

	public static Object getLocker()
	{
		return locker;
	}

	private Thread getCheckingThread()
	{
		return new Thread(() -> {
			try
			{
				if (!Data.canExecuteRootCommands) return;
				Data.busyboxLocations = rootInfo.findBinariesLocations(Data.BINARY_DIRECTORIES,
						"busybox");
				Data.suBinaries = rootInfo.getBinaries(Data.SU_DIRECTORIES);
				Data.superuser = rootInfo.getSuperuser();
				Data.superuserAppsFound = true;
				Data.linuxSESupported = rootInfo.isLinuxSEEnforcing();
				Data.busyboxFound = Data.busyboxLocations.length > 0;

				if (Data.busyboxFound)
				{
					try
					{
						Data.busyboxCommands = rootInfo.new GetBusyboxCommands().call();
					}
					catch (Exception e)
					{
						Data.busyboxCommands = new String[0];
					}
				}
			}
			finally
			{
				do
				{
					synchronized (locker)
					{
						locker.notifyAll();
					}
					try
					{
						Thread.sleep(100);
					}
					catch (InterruptedException ignored)
					{
						return;
					}
				}
				while (!rootNotifyReceived || !busyboxNotifyReceived);
			}
		});
	}

	@Override
	public void onResume()
	{
		super.onResume();
		navigation.setSelectedItemId(viewPager.getCurrentItem());

		checkRoot();
	}

	@Override
	public void onPause()
	{
		rootNotifyReceived = false;
		busyboxNotifyReceived = false;
		super.onPause();
	}

	@Override
	public void onBackPressed()
	{
		if (viewPager.getCurrentItem() == 0)
		{
			super.onBackPressed();
		}
		else
		{
			viewPager.setCurrentItem(lastFragmentIndex);
			lastFragmentIndex = 0;
		}

		navigation.setSelectedItemId(viewPager.getCurrentItem());
		navigation.getMenu().getItem(viewPager.getCurrentItem()).setChecked(true);
	}

	private ViewPager.SimpleOnPageChangeListener pageChangeListener
			= new ViewPager.SimpleOnPageChangeListener()
	{

		@Override
		public void onPageSelected(int selectedPage)
		{
			navigation.getMenu().getItem(selectedPage).setChecked(true);
		}
	};

	private class ViewPagerAdapter extends FragmentStatePagerAdapter
	{

		public ViewPagerAdapter(FragmentManager fm)
		{
			super(fm);
		}

		@Override
		public Fragment getItem(int pos)
		{
			switch (pos)
			{
				case 0:
				{
					return rootFragment;
				}
				case 1:
				{
					return busyboxFragment;
				}
				case 2:
				{
					return moreFragment;
				}
				default:
				{
					return rootFragment;
				}
			}
		}

		@Override
		public int getCount()
		{
			return 3;
		}

	}

	private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
			= new BottomNavigationView.OnNavigationItemSelectedListener()
	{

		@Override
		public boolean onNavigationItemSelected(@NonNull MenuItem item)
		{
			lastFragmentIndex = viewPager.getCurrentItem();
			switch (item.getItemId())
			{
				case R.id.navigation_root:
					viewPager.setCurrentItem(0);
					return true;
				case R.id.navigation_busybox:
					viewPager.setCurrentItem(1);
					return true;
				case R.id.navigation_info:
					viewPager.setCurrentItem(2);
					return true;
			}
			return false;
		}
	};
}
