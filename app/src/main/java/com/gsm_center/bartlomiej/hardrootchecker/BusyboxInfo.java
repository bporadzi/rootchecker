package com.gsm_center.bartlomiej.hardrootchecker;

/*
	Created by Bartek on 23.03.2018.
*/

public class BusyboxInfo
{

	String title;
	String message;

	String[] messages;

	public BusyboxInfo(String title, String message)
	{
		this.title = title;
		this.message = message;
		this.messages = null;
	}

	public BusyboxInfo(String title, String[] messages)
	{
		this.title = title;
		this.messages = messages;
		this.message = null;
	}

}
