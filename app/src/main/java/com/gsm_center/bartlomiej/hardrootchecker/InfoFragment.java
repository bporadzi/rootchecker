package com.gsm_center.bartlomiej.hardrootchecker;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gsm_center.bartlomiej.R;

import java.util.ArrayList;
import java.util.List;

public class InfoFragment extends Fragment
{

	private final static String INFO_CHECKER_PACKAGE
			= "com.gsm_center.bartlomiej.deviceinformation";
	private View view;

	public InfoFragment()
	{

	}

	@Override
	public void onResume()
	{
		super.onResume();
		view = getView();
	}

	@Override
	public void onPause()
	{
		view = null;
		super.onPause();
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_info, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);

		view = getView();

		if (view == null) return;

		registerListeners();

		RecyclerView rv = view.findViewById(R.id.recycler_view);

		LinearLayoutManager llm = new LinearLayoutManager(getContext());
		rv.setLayoutManager(llm);

		RVAdapter adapter = new RVAdapter(rv, getCards());
		rv.setAdapter(adapter);
	}

	private void registerListeners()
	{
		view.findViewById(R.id.card_view_info_checker).setOnClickListener(appsListener);
		view.findViewById(R.id.card_view_frp_unlocker).setOnClickListener(appsListener);
		view.findViewById(R.id.hardreset_link).setOnClickListener(appsListener);
	}

	private View.OnClickListener appsListener = v -> {
		switch (v.getId())
		{
			case R.id.card_view_info_checker:
			{
				try
				{
					startActivity(new Intent(Intent.ACTION_VIEW,
							Uri.parse("market://details?id=" + INFO_CHECKER_PACKAGE)));
				}
				catch (android.content.ActivityNotFoundException e)
				{
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(
							"https://play.google.com/store/apps/details?id="
									+ INFO_CHECKER_PACKAGE)));
				}
				return;
			}
			case R.id.card_view_frp_unlocker:
			{
				try
				{
					startActivity(new Intent(Intent.ACTION_VIEW,
							Uri.parse("http://www.hardreset.info/FRP-Unlocker/")));
				}
				catch (android.content.ActivityNotFoundException anfe)
				{
					anfe.printStackTrace();
				}
				return;
			}
			case R.id.hardreset_link:
			{
				try
				{
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(
							"http://www.hardreset.info/articles/all-you-need-to-know-about-rooting/")));
				}
				catch (android.content.ActivityNotFoundException anfe)
				{
					anfe.printStackTrace();
				}
			}
		}

	};

	private List <Card> getCards()
	{
		List <Card> cards = new ArrayList <>();

		cards.add(new Card("Root Definition", "Tap to read more",
				"Root, in the simplest definition, is the access to all of the Android's system "
						+ "files and the possibility to modify them. There is an application - SuperUser/SuperSU - which "
						+ "is used for managing those actions, as well as CWM (ClockWorkMod) and TWRP (Team Win Recovery Project) "
						+ "which are more advanced Recovery Modes than those in a standard Android devices. ",
				R.drawable.info_icon));
		cards.add(new Card("Advantages of Rooting", "Tap to read more",
				"The Root access enables a lot of hidden functions and advanced features such as a "
						+ "full backup of the system and the application or custom ROM installation. It allows you to "
						+ "uninstall any unwanted apps (e.g. those directly from the producer). What’s more you can also install "
						+ "applications on a memory card, make some system modifications or change the initial screen.",
				R.drawable.info_icon));
		cards.add(new Card("Disadvantages of Rooting", "Tap to read more",
				"By rooting, you are installing an unofficial operating system which will lead to loss of warranty. "
						+ "The Root may lead to the instability of the system and such firmware medication may attracts more viruses.",
				R.drawable.info_icon));
		return cards;
	}

}
