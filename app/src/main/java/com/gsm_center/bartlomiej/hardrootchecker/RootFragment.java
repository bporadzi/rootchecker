package com.gsm_center.bartlomiej.hardrootchecker;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gsm_center.bartlomiej.R;

import java.util.ArrayList;
import java.util.List;

public class RootFragment extends Fragment
{

	private RecyclerView rootValues;
	private RecyclerView.LayoutManager mLayoutManager;
	private RecyclerView.Adapter mAdapter;

	private Handler handler;

	Context context;

	private RootInfo rootInfo;

	private View view;

	public RootFragment()
	{
		handler = new Handler();
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		context = getContext();
		return inflater.inflate(R.layout.fragment_root, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceBundle)
	{
		super.onActivityCreated(savedInstanceBundle);

		view = getView();
		context = getContext();
		rootInfo = new RootInfo(context);


	}

	@Override
	public void onResume()
	{
		super.onResume();
		new Thread(this::displayInfo).start();
	}

	@Override
	public void onDestroy()
	{
		view = null;
		context = null;
		rootInfo = null;
		super.onDestroy();
	}

	private void displayInfo()
	{
		try
		{
			synchronized (MainActivity.getLocker())
			{
				MainActivity.getLocker().wait();
				MainActivity.rootNotifyReceived = true;
			}
		}
		catch (InterruptedException ignored) {}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		if (!Data.canExecuteRootCommands)
		{
			if (handler == null) return;
			handler.post(() -> {
				try
				{
					RelativeLayout contentLayout = view.findViewById(R.id.content);
					contentLayout.removeAllViews();
					getLayoutInflater().inflate(R.layout.layout_not_rooted, contentLayout);
					String link = getResources().getString(R.string.hardreset_link) + Build.MODEL;
					((TextView) view.findViewById(R.id.hardreset_link)).setText(link);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			});
			return;
		}

		try
		{
			handler.post(() -> {
				try
				{
					view.findViewById(R.id.image_root).setVisibility(View.VISIBLE);
					view.findViewById(R.id.message_rooted).setVisibility(View.VISIBLE);
				}
				catch (Exception ignored) {}
			});

			List <RootOption> rootOptions = new ArrayList <>();

			rootOptions.add(new RootOption("Root commands", Data.canExecuteRootCommands,
					"Root commands can" + (Data.canExecuteRootCommands ? "" : "not")
							+ " be executed"));
			rootOptions.add(
					new RootOption("Superuser Apps", Data.superuserAppsFound, Data.superuser));
			rootOptions.add(
					new RootOption("SU Binaries", Data.canExecuteRootCommands, Data.suBinaries));
			rootOptions.add(new RootOption("Build contains 'test-key'", rootInfo.isTestKeyBuild(),
					(rootInfo.isTestKeyBuild() ? "Found test-key in system build" :
							"Not found test-keys in system build")));
			rootOptions.add(new RootOption("SELinux", Data.linuxSESupported,
					Data.linuxSESupported ? "Enforcing" : "Permissive"));

			mAdapter = new RootAttributesAdapter(context, rootOptions);

			handler.post(() -> {
				try
				{
					rootValues = view.findViewById(R.id.root_values);
					rootValues.setHasFixedSize(true);
					mLayoutManager = new LinearLayoutManager(context);
					rootValues.setLayoutManager(mLayoutManager);
					mAdapter.notifyDataSetChanged();
					rootValues.setAdapter(mAdapter);
				}
				catch (Exception ignored) { }
			});
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
