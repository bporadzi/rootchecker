package com.gsm_center.bartlomiej.hardrootchecker;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/*
    Created by Bartłomiej on 12.01.2018
*/

public class RootInfo
{

	private Context context;

	public RootInfo(Context context)
	{
		this.context = context;
	}

	public boolean canExecuteSuCommand()
	{
		try
		{
			Process suProcess = Runtime.getRuntime().exec("su");
			DataOutputStream dataOutputStream = new DataOutputStream(suProcess.getOutputStream());
			DataInputStream dataInputStream = new DataInputStream(suProcess.getInputStream());

			dataOutputStream.writeBytes("id\n");
			dataOutputStream.flush();

			String currUid = dataInputStream.readLine();

			boolean suGranted;

			boolean exitSu;

			if (currUid == null)
			{
				suGranted = false;
				exitSu = false;
				Toast.makeText(context, "Can't get SU access or rejected by user",
						Toast.LENGTH_LONG).show();
			}
			else if (currUid.contains("uid=0"))
			{
				suGranted = true;
				exitSu = true;
				Toast.makeText(context, "Root access granted", Toast.LENGTH_LONG).show();
			}
			else
			{
				suGranted = false;
				exitSu = true;
				Toast.makeText(context, "Root access rejected", Toast.LENGTH_LONG).show();
			}

			if (exitSu)
			{
				dataOutputStream.writeBytes("exit\n");
				dataOutputStream.flush();
			}

			return suGranted;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	//	public boolean findBinary(String[] directories, String binaryName)
	//	{
	//		for (String where : directories)
	//		{
	//			if (new File(where + binaryName).exists())
	//			{
	//				return true;
	//			}
	//		}
	//		try
	//		{
	//			File[] files = new File("/data/app").listFiles();
	//
	//			for (File file : files)
	//			{
	//				if (file.getName().contains("supersu"))
	//				{
	//					return true;
	//				}
	//			}
	//		}
	//		catch (Exception e)
	//		{
	//			e.printStackTrace();
	//		}
	//		return false;
	//	}

	public class GetBusyboxCommands implements Callable <String[]>
	{

		@Override
		public String[] call() throws Exception
		{

			Process process = Runtime.getRuntime().exec("busybox");

			BufferedReader reader = new BufferedReader(
					new InputStreamReader(process.getInputStream()));

			int read;
			char[] buffer = new char[4096];
			StringBuilder output = new StringBuilder();
			while ((read = reader.read(buffer)) > 0)
			{
				output.append(buffer, 0, read);
			}
			reader.close();

			process.waitFor();

			String pattern = "Currently defined functions:";

			String commandsString = output.toString().substring(
					output.indexOf("Currently defined functions:") + pattern.length());

			return commandsString.split(",");
		}
	}

	public String[] findBinariesLocations(String[] directories, String binaryName)
	{
		List <String> directoriesDetected = new ArrayList <>();

		for (String where : directories)
		{
			if (new File(where + binaryName).exists())
			{
				directoriesDetected.add(where);
			}
		}

		return /*directoriesDetected.size() != 0 ?*/ directoriesDetected.toArray(
				new String[0]); //: null;
	}

	public String[] getBinaries(String[] directories)
	{
		List <String> directoriesDetected = new ArrayList <>();

		for (String directory : directories)
		{
			File file = new File(directory);

			if (file.exists()) directoriesDetected.add(directory);
		}

		return directoriesDetected.toArray(new String[0]);
	}

	public String findSuperuserApps()
	{
		String[] packages = {"com.noshufou.android.su", "com.thirdparty.superuser",
				"eu.chainfire.supersu", "com.koushikdutta.superuser",
				"com.zachspong.temprootremovejb", "com.ramdroid.appquarantine"
		};

		for (String pack : packages)
		{
			if (isAppInstalled(pack)) return pack;
		}

		return null;
	}

	public boolean isLinuxSEEnforcing()
	{
		try
		{
			Process process = Runtime.getRuntime().exec("getenforce");

			BufferedReader reader = new BufferedReader(
					new InputStreamReader(process.getInputStream()));

			int read;
			char[] buffer = new char[4096];
			StringBuilder output = new StringBuilder();
			while ((read = reader.read(buffer)) > 0)
			{
				output.append(buffer, 0, read);
			}
			reader.close();

			process.waitFor();

			return output.toString().toLowerCase().contains("enforcing");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}

	public String getSuperuser()
	{
		try
		{
			String superuser = findSuperuserApps();

			switch (superuser)
			{
				case "com.noshufou.android.su":
				{
					return "Superuser by ChainsDD";
				}
				case "com.thirdparty.superuser":
				{
					return "Superuser";
				}
				case "eu.chainfire.supersu":
				{
					return "SuperSU by Chainfire";
				}
				case "com.koushikdutta.superuser":
				{
					return "Superuser by Koush";
				}
				case "com.zachspong.temprootremovejb":
				{
					return "Superuser by Zachspong";
				}
				case "com.ramdroid.appquarantine":
				{
					return "SuperSU by Ramdroid";
				}
				default:
				{
					return null;
				}
			}
		}
		catch (NullPointerException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public boolean isAppInstalled(String packageName)
	{
		PackageManager packageManager = context.getPackageManager();
		try
		{
			packageManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}


	public boolean isTestKeyBuild()
	{
		String str = Build.TAGS;
		return ((str != null) && (str.contains("test-keys")));
	}
}
