package com.gsm_center.bartlomiej.hardrootchecker;

public class Card
{

	String title;
	String message;
	int photoId;
	String fullArticle;

	Card(String title, String message, String fullArticle, int photoId)
	{
		this.title = title;
		this.message = message;
		this.fullArticle = fullArticle;
		this.photoId = photoId;
	}

}

