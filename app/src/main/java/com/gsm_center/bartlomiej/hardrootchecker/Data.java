package com.gsm_center.bartlomiej.hardrootchecker;

public class Data
{

	public static String[] SU_DIRECTORIES = {"/system/bin/su", "/system/xbin/su", "/sbin/su",
			"/system/su", "/system/xbin/mu", "/system/usr/we-need-root/su-backup"
	}; //"/system/bin/.ext/.su"
	public static String[] BINARY_DIRECTORIES = {"/sbin/", "/system/bin/", "/system/xbin/",
			"/data/local/xbin/", "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/",
			"/data/local/"
	};

	public static boolean linuxSESupported;
	public static boolean busyboxFound;
	public static String[] busyboxCommands;
	public static boolean superuserAppsFound;
	public static String[] suBinaries;
	public static String[] busyboxLocations;
	public static String superuser;
	public static boolean canExecuteRootCommands;
}
