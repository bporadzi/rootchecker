package com.gsm_center.bartlomiej.hardrootchecker;

/*
	Created by Bartłomiej on 10.10.2017.
 */

public class RootOption
{

	private String valueName;
	private boolean value;
	private String additionalInfo;
	private String[] valuesArray;

	public RootOption(String valueName, boolean value)
	{
		this.valueName = valueName;
		this.value = value;
		this.additionalInfo = "";
		valuesArray = null;
	}

	public RootOption(String valueName, boolean value, String additionalInfo)
	{
		this(valueName, value);
		this.additionalInfo = additionalInfo;
	}

	public RootOption(String valueName, boolean value, String[] valuesArray)
	{
		this(valueName, value);
		this.valuesArray = valuesArray;
	}

	public boolean isArrayNull()
	{
		return valuesArray == null;
	}

	public String[] getAdditionalValues()
	{
		return valuesArray;
	}

	public String getValueName()
	{
		return valueName;
	}

	public boolean getValue()
	{
		return value;
	}

	public String getAdditionalInfo()
	{
		return additionalInfo;
	}
}
