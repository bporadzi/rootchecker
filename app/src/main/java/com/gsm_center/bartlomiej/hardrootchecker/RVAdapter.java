package com.gsm_center.bartlomiej.hardrootchecker;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gsm_center.bartlomiej.R;

import java.util.List;

/*
	Created by Bartek on 22.03.2018.
*/

public class RVAdapter extends RecyclerView.Adapter <RVAdapter.ArticleViewHolder>
{

	List <Card> cards;
	RecyclerView recyclerView;

	RVAdapter(RecyclerView recyclerView, List <Card> cards)
	{
		this.cards = cards;
		this.recyclerView = recyclerView;
	}

	@Override
	public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
	{
		super.onAttachedToRecyclerView(recyclerView);
	}

	@NonNull
	@Override
	public ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
	{
		View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent,
				false);
		v.setOnClickListener(cardClickListener);
		return new ArticleViewHolder(v);
	}

	@Override
	public void onBindViewHolder(@NonNull ArticleViewHolder holder, int position)
	{
		Card card = cards.get(position);
		holder.articleTitle.setText(card.title);
		holder.articleMessage.setText(card.message);
		holder.articlePhoto.setImageResource(card.photoId);
		holder.fullArticle.setText(card.fullArticle);
	}

	@Override
	public int getItemCount()
	{
		return cards.size();
	}

	private View.OnClickListener cardClickListener = new View.OnClickListener()
	{

		@RequiresApi(api = Build.VERSION_CODES.KITKAT)
		@Override
		public void onClick(View v)
		{
			View fullDescription = v.findViewById(R.id.full_article);
			View readMore = v.findViewById(R.id.article_message);

			fullDescription.setVisibility(
					fullDescription.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
			readMore.setVisibility(
					readMore.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);

			TransitionManager.beginDelayedTransition(recyclerView);
			notifyDataSetChanged();
		}
	};

	public static class ArticleViewHolder extends RecyclerView.ViewHolder
	{

		CardView cv;
		TextView articleMessage;
		TextView articleTitle;
		ImageView articlePhoto;
		TextView fullArticle;

		ArticleViewHolder(View itemView)
		{
			super(itemView);
			cv = itemView.findViewById(R.id.cv);
			articleMessage = itemView.findViewById(R.id.article_message);
			articleTitle = itemView.findViewById(R.id.article_title);
			articlePhoto = itemView.findViewById(R.id.article_photo);
			fullArticle = itemView.findViewById(R.id.full_article);
		}

	}

}
