package com.gsm_center.bartlomiej.hardrootchecker;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.gsm_center.bartlomiej.R;

import java.util.ArrayList;
import java.util.List;

public class BusyBoxFragment extends Fragment
{

	private View view;
	private Handler handler;

	public BusyBoxFragment()
	{
		handler = new Handler();
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_busy_box, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceBundle)
	{
		super.onActivityCreated(savedInstanceBundle);
		view = getView();

	}

	@Override
	public void onResume()
	{
		super.onResume();
		new Thread(this::displayInfo).start();
	}

	private void displayInfo()
	{
		try
		{
			synchronized (MainActivity.getLocker())
			{
				MainActivity.getLocker().wait();
				MainActivity.busyboxNotifyReceived = true;
			}
		}
		catch (InterruptedException ignored) { }
		catch (Exception e)
		{
			e.printStackTrace();
		}

		try
		{

			if (Data.busyboxLocations == null || Data.busyboxLocations.length == 0)
			{
				if (view == null)
				{
					return;
				}

				RelativeLayout container = view.findViewById(R.id.busybox_content);

				handler.post(() -> {
					try
					{
						container.removeAllViews();
						getLayoutInflater().inflate(R.layout.no_busybox, container);
					}
					catch (Exception ignored) {}
				});
				return;
			}

			if (view == null || handler == null) return;

			handler.post(() -> {
				try
				{
					RecyclerView rv = view.findViewById(R.id.busybox_recycler_view);

					LinearLayoutManager llm = new LinearLayoutManager(getContext());
					rv.setLayoutManager(llm);

					List <BusyboxInfo> busyboxInfos = new ArrayList <>();
					busyboxInfos.add(new BusyboxInfo("Busybox", "properly installed"));
					busyboxInfos.add(new BusyboxInfo("Busybox location", Data.busyboxLocations));
					busyboxInfos.add(new BusyboxInfo("Busybox commands", Data.busyboxCommands));

					BusyboxAdapter adapter = new BusyboxAdapter(busyboxInfos);
					rv.setAdapter(adapter);
					adapter.notifyItemInserted(0);
					adapter.notifyItemInserted(1);
					adapter.notifyItemInserted(2);
				}
				catch (Exception ignored) { }
			});
		}
		catch (Exception e)
		{
			System.out.println("dziwny exception");
			e.printStackTrace();
		}

	}
}
