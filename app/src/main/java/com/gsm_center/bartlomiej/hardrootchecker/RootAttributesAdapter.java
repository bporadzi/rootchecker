package com.gsm_center.bartlomiej.hardrootchecker;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.TextView;

import com.gsm_center.bartlomiej.R;

import java.util.List;

/**
 * Created by Bartłomiej on 06.10.2017
 */

public class RootAttributesAdapter extends RecyclerView.Adapter <RootAttributesAdapter.ViewHolder>
{

	private List <RootOption> rootOptions;
	private int lastPosition = -1;
	Context context;

	public RootAttributesAdapter(Context context, List <RootOption> rootOptions)
	{
		this.rootOptions = rootOptions;
		this.context = context;
	}

	public static class ViewHolder extends RecyclerView.ViewHolder
	{

		private TextView textViewName;
		private CheckBox value;
		private TextView additionalInfo;

		public ViewHolder(View v)
		{
			super(v);
			textViewName = v.findViewById(R.id.name_of_value);
			value = v.findViewById(R.id.value);
			additionalInfo = v.findViewById(R.id.additional_info);
		}
	}

	@NonNull
	@Override
	public RootAttributesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
			int viewType)
	{
		View itemView = LayoutInflater.from(parent.getContext()).inflate(
				R.layout.list_row_root_option, parent, false);

		return new ViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position)
	{
		RootOption option = rootOptions.get(position);
		holder.textViewName.setText(option.getValueName());
		holder.value.setChecked(option.getValue());

		if (option.isArrayNull())
		{
			holder.additionalInfo.setText(option.getAdditionalInfo());
		}
		else
		{
			String[] values = option.getAdditionalValues();
			if (values == null || values.length == 0)
			{
				return; //values == null
			}

			StringBuilder tmp = new StringBuilder();
			int lastIndex = values.length - 1;
			for (int i = 0; i < lastIndex; i++)
			{
				tmp.append(values[i]);
				tmp.append("\n");
			}
			tmp.append(values[lastIndex]);
			holder.additionalInfo.setText(tmp);
		}
		setAnimation(holder.itemView, position);
	}

	protected void setAnimation(View viewToAnimate, int position)
	{
		if (position > lastPosition)
		{
			Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
			viewToAnimate.startAnimation(animation);
			lastPosition = position;
		}
	}

	@Override
	public int getItemCount()
	{
		return rootOptions.size();
	}
}
